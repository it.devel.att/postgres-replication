### Частично по руководству https://www.dmosk.ru/miniinstruktions.php?mini=postgresql-replication
> C адаптацией под докер

#### TODO Вынести всё в init скрипты


На мастере
```
su postgres
createuser --replication -P repluser
```
> Задать пароль юзеру

На слейвe
> Перед этим наверняка прийдётся очистить `/var/lib/postgresql/data`
```
rm -r /var/lib/postgresql/data/*
su - postgres -c "pg_basebackup --host=pg-master --username=repluser --pgdata=/var/lib/postgresql/data --wal-method=stream --write-recovery-conf"
```
> ввести пароль заданый на мастере

После этого надо рестартануть контейнер со слевом

Проверка статуа репликации на мастере 
```
select * from pg_stat_replication;
```

Проверка статуса репликации на слейве
```
select * from pg_stat_wal_receiver;
```

#### Проверка репликации
На мастере создадим табличку и нальём туда записей

```
CREATE TABLE IF NOT EXISTS test_numbers (id BIGSERIAL PRIMARY KEY, number NUMERIC DEFAULT 0 NOT NULL);
```
Нальём записей
```
INSERT INTO test_numbers(number) SELECT * FROM generate_series(1, 1000);
```

На слейве проверим что таблица и данные в ней существуют
```
SELECT count(id) FROM test_numbers;
```
